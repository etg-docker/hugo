# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [Unreleased]



## [1.3.0-0.140.2] 2025-01-04

### Added

- convenience scripts for server and build

### Changed

- using official hugo docker image #6
	- project dir changed from `/src` to `/project`
- hugo: `0.140.2`
- test build script now without external dependencies

### Fixed

- changelog syntax

### Removed

- dependencies for obsolete theme #3


## [1.2.0-0.111.3] 2023-07-14

### Changed

- hugo: `0.111.3` #5


## [1.1.0-0.107.0] 2023-04-06

### Changed

- hugo: `0.107.0`
- using alpine docker image as base instead of ubuntu image: `klakegg/hugo:${HUGO_VERSION}-alpine` #2


## [1.0.0-0.101.0] 2022-12-29

- initial version #1
- hugo: `0.101.0`

### Added

- hugo docker image
- test files
- CI/CD for gitlab


[Unreleased]: https://gitlab.com/etg-docker/hugo/-/compare/1.3.0-0.140.2...HEAD
[1.3.0-0.140.2]: https://gitlab.com/etg-docker/hugo/-/compare/1.2.0-0.111.3...1.3.0-0.140.2
[1.2.0-0.111.3]: https://gitlab.com/etg-docker/hugo/-/compare/1.1.0-0.107.0...1.2.0-0.111.3
[1.1.0-0.107.0]: https://gitlab.com/etg-docker/hugo/-/compare/1.0.0-0.101.0...1.1.0-0.107.0
[1.0.0-0.101.0]: https://gitlab.com/etg-docker/hugo/-/tags/1.0.0-0.101.0

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
