source ../settings.sh

echo "Shows version."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	$IMAGENAME \
		version
