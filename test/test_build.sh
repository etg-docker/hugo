source ../settings.sh

echo "Create and build test website."
echo

PATH_THEMES=themes
TESTTHEME=testtheme
THEME_GIT_REPO=https://github.com/halogenica/beautifulhugo.git

# remove test website if exists
# cannot use --recursive (-r) and --force (-f) because of shell (sh) in docker:dind
rm -rf $PATH_THEMES

# create test theme
docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/project" \
	--publish 1313:1313 \
	$IMAGENAME \
		new theme $TESTTHEME

# build test themes website - should run without errors or warnings
docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/project" \
	--publish 1313:1313 \
	$IMAGENAME \
		--cleanDestinationDir \
		--source "$PATH_THEMES/$TESTTHEME"
